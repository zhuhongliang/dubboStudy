package com.zhl.service; 
/** 
* @author 朱宏亮
* @version 创建时间：2018年1月9日 下午5:39:17 
* 类说明 
*/
public interface UserService {

    void setUser(User user); 
    
    User getUser();
}
 