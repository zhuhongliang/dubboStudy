package com.zhl.service;

import java.io.Serializable;

/** 
* @author 朱宏亮
* @version 创建时间：2018年1月9日 下午5:37:38 
* 类说明 
*/
public class User implements Serializable{

    private static final long serialVersionUID = 1L;
    private int id;
    private String name;
    private String phone;
    private String address;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
   
}
 