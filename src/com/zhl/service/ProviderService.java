package com.zhl.service; 
/** 
* @author 朱宏亮
* @version 创建时间：2018年1月10日 上午11:01:25 
* 类说明 
*/
public interface ProviderService {
    
    public String hello(String name);
    
    public String say(String name);
    
}
 