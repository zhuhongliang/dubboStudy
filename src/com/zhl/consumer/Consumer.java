package com.zhl.consumer;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zhl.service.DemoService;
import com.zhl.service.ProviderService;
import com.zhl.service.User;
import com.zhl.service.UserService;

/** 
* @author 朱宏亮
* @version 创建时间：2018年1月9日 上午10:10:33 
* 类说明 
*/
public class Consumer {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"conf/consumer.xml"});
        //for(int i =0;i <10;i++){
        context.start();
       /* // obtain proxy object for remote invocation
        DemoService demoService = (DemoService) context.getBean("demoService");
        // execute remote invocation
        String hello = demoService.sayHello("world");
        // show the result
        System.out.println(hello);
        */
        UserService userService = (UserService) context.getBean("userService");
        User user = userService.getUser();
        System.out.println("id:"+user.getId()+"\t name:"+user.getName()+"\t phone:"+user.getPhone()+"\t address:"+user.getAddress());
        user.setId(88);
        user.setName("朱宏亮");
        user.setPhone("18627888889");
        user.setAddress("中关村软件园");
        userService.setUser(user);
        User user2 = userService.getUser();
        System.out.println("user2 id:"+user2.getId()+"\t user2 name:"+user2.getName()+"\t user2 phone:"+user2.getPhone()+"\t user2 address:"+user2.getAddress());

        ProviderService providerService = (ProviderService)context.getBean("providerService");
        String msg = providerService.hello("zhl");
        System.out.println(msg);
        String msg2 = providerService.say("朱宏亮");
        System.out.println(msg2);
        
        
        //}
        //context.close();
        //System.exit(0);
    }

}
 